## Build stage
FROM python:3.6-alpine as build

COPY requirements.txt /requirements.txt
RUN apk add libpq-dev && apk add build-base && pip install -r requirements.txt

## Runtime stage
FROM python:3.6-alpine as flask_app
EXPOSE 5000
## Copying neccesary files for run from build stage
COPY --from=build /usr/local /usr/local
COPY --from=build /usr/lib/libpq.so.5 /usr/lib/libpq.so.5
COPY . /app
WORKDIR /app
RUN chmod u+x entrypoint.sh
ENTRYPOINT [ "./entrypoint.sh" ]